# RUSTICO: RobUST Inhibition-augmented Curvilinear Operator
This is the official repository of RUSTICO, an operator for delineation of curvilinear structures in images.

RUSTICO is brain-inspired as it implements a push-pull inhibition mechanism shown by neurons in area V1 of visual system [1,2]. RUSTICO is built upon the trainable B-COSFIRE filter approach [2] and is robust to background noise, in the form of spurious textured patterns. It significantly improves on the B-COSFIRE filter performance.

This repository containes the following folders:
* __data__: data sets used for evaluation (we do not include not-authorized third-party data sets)
* __examples__: code examples of how to configure and apply RUSTICO
* __experiments__: code to replicate the experiments reported in [1]
* __results__: segmented output images obtained for the data sets considered in [1]
* __src__: Matlab source code 
    * __RUSTICO__: source code of RUSTICO
    * __utils__: utility scripts (pre-processing, performance evaluation, etc.)

## Getting started
In the folder _src/examples_, there are Matlab scripts that show how to configure and use RUSTICO to detect structures of interest in images.  

## Data sets
RUSTICO was tested on several benchmark data sets for evaluation of detection and segmentation of curvilinear structures in images.  

Data sets available in the _data_ folder:
* TB-roses (official repository)
* Road
* River

Data sets to download:
* DRIVE
* STARE
* CHASE_DB1
* HRF
* CrackTree206
* Crack_ivc
* Crack_PV14

Please, find more information and instructions in [the data set README file](/data) 


## Experiment results
In the folder _experiments_, there are the Matlab scripts to replicate the experiments reported in [1].  
For details about how to run the experiments, please refer at the [README](/experiments) file in the folder _experiments_.  
 
## Acknowledgements
The development of RUSTICO was partially supported by the EU H2020 research and innovation program, grant no. 688007 (TrimBot2020).
Please, visit the [website of the TrimBot2020 project](http://www.trimbot2020.org).

The code was developed and is maintained by Nicola Strisciuglio (http://www.nicolastrisciuglio.eu).

### References (and bibtex)
[1] N. Strisciuglio, G. Azzopardi, N. Petkov, __Robust Inhibition-augmented Operator for Delineation of Curvilinear Structures__, _IEEE Transactions on Image Processing, 2019_, to appear

[2] N. Strisciuglio, G. Azzopardi, N. Petkov, __Brain-inspired robust delineation operator__, _The European Conference on Computer Vision (ECCV) Workshops, 2018_

	@InProceedings{StrisciuglioBDCV2018,
	author="Strisciuglio, Nicola and Azzopardi, George and Petkov, Nicolai",
	editor="Leal-Taix{\'e}, Laura and Roth, Stefan",
	title="Brain-Inspired Robust Delineation Operator",
	booktitle="Computer Vision -- ECCV 2018 Workshops",
	year="2019",
	publisher="Springer International Publishing",
	address="Cham",
	pages="555--565"
	}

[3] G. Azzopardi, N. Strisciuglio, M. Vento, N. Petkov,  __Trainable COSFIRE filters for vessel delineation with application to retinal images__, _Medical Image Analysis , Volume 19 , Issue 1 , 46 - 57_

	@article{BCOSFIRE-MedIA2015,
	title = "Trainable {COSFIRE} filters for vessel delineation with application to retinal images ",
	journal = "Medical Image Analysis ",
	volume = "19",
	number = "1",
	pages = "46 - 57",
	year = "2015",
	note = "",
	issn = "1361-8415",
	doi = "http://dx.doi.org/10.1016/j.media.2014.08.002",
	author = "George Azzopardi and Nicola Strisciuglio and Mario Vento and Nicolai Petkov",
	} 