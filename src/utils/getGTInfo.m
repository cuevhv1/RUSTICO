function [SG, gtdil, gtthin] = getGTInfo(image, gt, mask)
%     if nargin == 2
%         R = image(:,:,1);
%         G = image(:,:,2);
%         B = image(:,:,3);
% 
%         [L,~,~] = RGB2Lab(R,G,B);
%         L = L ./ 100;
%         mask = 1 - (L < 0.5);
%     end

    bw = bwlabel(gt);
    stats_gt = regionprops(bw);
    SG = numel(stats_gt);

    gtdil = imdilate(gt,strel('disk', 2));
    gtthin = bwmorph(gt, 'thin', inf);