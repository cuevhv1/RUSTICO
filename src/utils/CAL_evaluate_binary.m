function [CAL, C, A, L] = CAL_evaluate_binary(resp, gt, mask, SG, gtdil, gtthin)
    % See paper 'A Function for Quality Evaluation of Retinal Vessel Segmentations'
    binoutput = resp .* mask;
    bw = bwlabel(binoutput);
    stats = regionprops(bw);

    % Connectivity
    nc = numel(stats);
    C = 1 - min(1,(abs(SG-nc)/sum(gt(:))));

    % Area
    respdil = imdilate(binoutput,strel('disk',2));
    item1 = gt == respdil & gtdil == 1 & respdil == 1;
    item2 = gtdil == binoutput & gtdil == 1 & binoutput == 1;
    A = sum(sum(item1 | item2))/sum(sum(gt | binoutput));

    % Length
    respthin = bwmorph(binoutput, 'thin', inf);
    L = sum(sum((respthin & gtdil) | (respdil & gtthin)))/sum(sum(respthin | gtthin));

    % CAL
    CAL = C * A * L;
