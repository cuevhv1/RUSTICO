function [binarymap] = binarize(rotoutput1, highthresh)
    %%%%%%%%%%%%%%%%% BEGIN BINARIZATION %%%%%%%%%%%%%%%%%%
    % compute thinning
    L = size(rotoutput1, 3);
    orienslist = 0:pi/L:pi-pi/L;
    [viewResult, oriensMatrix] = calc_viewimage(rotoutput1,1:numel(orienslist), orienslist);
    thinning = calc_thinning(viewResult, oriensMatrix, 1);
    %figure; imagesc(thinning);
    % 
    % % Choose high threshold of hysteresis thresholding
    % if nargin == 4
    %     bins = 64;p = 0.05; %Keep the strongest 10% of the pixels in the resulting thinned image
    %     f = find(thinning > 0);
    %     counts = imhist(thinning(f),bins);
    %     highthresh = find(cumsum(counts) > (1-p)*length(f),1,'first') / bins;
    % end
    % 
    binarymap = calc_hysteresis(thinning, 1, 0.5*highthresh*max(thinning(:)), highthresh*max(thinning(:)));
    %figure;imagesc(binarymap);colormap gray; axis image;
    % show binarized image
    % figure;
    % subplot(1,2,1);imagesc(img);axis off;axis image;colormap(gray);
    % subplot(1,2,2);imagesc(imcomplement(binarymap));axis off;axis image;colormap(gray);
    %%%%%%%%%%%%%%%%% END BINARIZATION %%%%%%%%%%%%%%%%%%%%