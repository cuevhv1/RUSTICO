function filterset = configureRUSTICO(featureImage, keypoint, params)
    % Get responses of a Bank of DoG filters
    input = getDoGBankResponse(featureImage, params.inputfilter.DoG);
    % Half-wave rectification / ReLU
    input(input < params.COSFIRE.t1 * max(input(:))) = 0;    

    push.tuples = configureTuples(input, keypoint, params);    
    push.params = params;
    
    pull = push;
    pull.tuples(1,:) = 1 - pull.tuples(1, :);
    pull.tuples(2,:) = pull.tuples(2, :) * params.RUSTICO.lambda;
    
    % Save excitatory (push) and inhibitory (pull) components
    filterset{1} = push;
    filterset{2} = pull;
end

function tuples = configureTuples(input, keypoint, params)
    tuples = [];
    [sz(1), sz(2), sz(3), sz(4)] = size(input);
    maxrho = max(params.COSFIRE.rholist);
    supportRadius = ceil(maxrho + (params.COSFIRE.sigma0 + (params.COSFIRE.alpha * maxrho)) * 3);

    % Enlarge the input area
    biginput = zeros(sz(1) + (2 * supportRadius), sz(2) + (2 * supportRadius), sz(3), sz(4));
    biginput(supportRadius+1:end-supportRadius, supportRadius+1:end-supportRadius, :, :) = input;
    keypoint = keypoint + supportRadius;

    biginput(biginput < params.COSFIRE.t2 * max(biginput(:))) = 0;
    maxInput = max(reshape(biginput,sz(1)+(2 * supportRadius),sz(2)+(2 * supportRadius),prod(sz(3:end))),[],3);

    for r = 1:length(params.COSFIRE.rholist)  
        tuple = getTuples(biginput, maxInput, keypoint, params.COSFIRE.rholist(r), params);
        tuples = [tuples [tuple.param1;tuple.param2;repmat(params.COSFIRE.rholist(r),1,length(tuple.phi));tuple.phi]];
    end
end
    
function tuple = getTuples(input, maxInput, keypoint, rho, params)
    if rho == 0
    
        centreResponses = squeeze(input(keypoint(1), keypoint(2), :, :));
        [polarityMax, sigmaIndex] = max(centreResponses, [], 1);
        polarityIndex = find(polarityMax);
        tuple.param1 = params.inputfilter.DoG.polaritylist(polarityIndex);
        tuple.param2 = params.inputfilter.DoG.sigmalist(sigmaIndex(polarityIndex));        
        tuple.phi = zeros(1,length(tuple.param2));        
    elseif rho > 0
        tuple.param1 = [];
        tuple.param2 = [];
        tuple.phi    = [];

        philist = (1:360) * pi / 180;
        resp_along_circle = maxInput(sub2ind(size(maxInput),round(keypoint(1) - rho*sin(philist)),round(keypoint(2) + rho*cos(philist))));

        if length(unique(resp_along_circle)) == 1
            % The input along the circle of the given radius rho is constant      
            return;
        end

        BW = bwlabel(imregionalmax(resp_along_circle));
        npeaks = max(BW(:));
        peaks = zeros(1, npeaks);
        for i = 1:npeaks
            peaks(i) = floor(mean(find(BW == i)));
        end    

        peaklist = zeros(1,length(philist));
        peaklist(peaks) = resp_along_circle(peaks);
        [peaklist, peaklocs] = findpeaks(peaklist, 'minpeakdistance', round(params.COSFIRE.eta*180/pi));
        [phivalues, uidx] = unique(mod(philist(peaklocs), 2*pi));  
        if isempty(phivalues)              
            return;
        end
        if phivalues(1) + (2 * pi) - phivalues(end) < params.COSFIRE.eta
            if peaklist(uidx(1)) <= peaklist(uidx(end))
                phivalues(1) = [];
            else
                phivalues(end) = [];
            end
        end
        [x, y] = pol2cart(phivalues,rho);    
        for i = 1:length(phivalues)
            responses = squeeze(input(keypoint(1 ) -round(y(i)), keypoint(2) + round(x(i)), :, :));
            [polarityMax, sigmaIndex] = max(responses, [], 1);
            polarityIndex = find(polarityMax);            
            tuple.param1 = [tuple.param1 params.inputfilter.DoG.polaritylist(polarityIndex)];
            tuple.param2 = [tuple.param2 params.inputfilter.DoG.sigmalist(sigmaIndex(polarityIndex))];            
            tuple.phi    = [tuple.phi repmat(phivalues(i),1,length(polarityIndex))];            
        end    
    end
end